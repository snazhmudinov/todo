import org.gradle.api.artifacts.dsl.DependencyHandler

object Deps {

    const val compileSdkVersion = 29
    const val midSdkVersion = 16
    const val targetSdkVersion = 29
    const val buildToolVersion = "29.0.2"

    var versionCode = 1
    const val versionName = "1.0"

    const val gradleVersion = "3.5.0"
    const val kotlinVersion = "1.3.50"

    const val BEN_MANES_DEPENDENCY_CHECK_VER = "0.25.0"

    object AppDeps {
        private const val EVENT_BUS_VER = "3.1.1"
        private const val APP_COMP_VER = "1.1.0"
        private const val CONSTRAINT_LAYOUT_VER = "1.1.3"
        private const val KTX_VERSION = "1.1.0"
        const val COROUTINES_VER = "1.3.1"
        private const val RECYCLER_VIEW_VER = "1.0.0"
        private const val MATERIAL_VER = "1.0.0"
        private const val LEAK_CANARY_VER = "1.6.3"
        private const val TIMBER_VER = "4.7.1"
//        private const val RX_BINDING_VER = "3.0.0"
        private const val CORBIND_VER = "1.1.2"

        const val EVENT_BUS = "org.greenrobot:eventbus:$EVENT_BUS_VER"
        const val APP_COMPAT = "androidx.appcompat:appcompat:$APP_COMP_VER"
        const val RECYCLER_VIEW = "androidx.recyclerview:recyclerview:$RECYCLER_VIEW_VER"
        const val MATERIAL = "com.google.android.material:material:$MATERIAL_VER"
        const val CONSTRAINT_LAYOUT = "androidx.constraintlayout:constraintlayout:$CONSTRAINT_LAYOUT_VER"
        const val KTX = "androidx.core:core-ktx:$KTX_VERSION"
        const val COROUTINES_CORE = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$COROUTINES_VER"
        const val COROUTINES_ANDROID = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$COROUTINES_VER"
        const val LEAK_CANARY_ANDROID = "com.squareup.leakcanary:leakcanary-android:$LEAK_CANARY_VER"
        const val LEAK_CANARY_NO_OP = "com.squareup.leakcanary:leakcanary-android-no-op:$LEAK_CANARY_VER"
        const val TIMBER = "com.jakewharton.timber:timber:$TIMBER_VER"
//        const val RX_BINDING_CORE = "com.jakewharton.rxbinding3:rxbinding-core:$RX_BINDING_VER"
//        const val RX_BINDING_APP_COMPAT = "com.jakewharton.rxbinding3:rxbinding-appcompat:$RX_BINDING_VER"
        const val CORBIND_CORE = "ru.ldralighieri.corbind:corbind-core:$CORBIND_VER"
        const val CORBIND_APPCOMPAT = "ru.ldralighieri.corbind:corbind-appcompat:$CORBIND_VER"
    }

    object ArchCompDeps {
        private const val LIFECYCLE_VER = "2.1.0"
        private const val LIFECYCLE_KTX_VER = "2.1.0"
        private const val LIFECYCLE_RUNTIME_KTX_VER = "2.2.0-alpha02"
        private const val LIVE_DATA_KTX_VER = "2.2.0-alpha02"
        private const val ROOM_VER = "2.1.0"

        const val VIEW_MODEL = "androidx.lifecycle:lifecycle-viewmodel:$LIFECYCLE_VER"
        const val VIEW_MODEL_KTX = "androidx.lifecycle:lifecycle-viewmodel-ktx:$LIFECYCLE_KTX_VER"
        const val LIVE_DATA = "androidx.lifecycle:lifecycle-extensions:$LIFECYCLE_VER"
        const val LIVE_DATA_KTX = "androidx.lifecycle:lifecycle-livedata-ktx:$LIVE_DATA_KTX_VER"
        const val LIFECYCLE_RUNTIME_KTX = "androidx.lifecycle:lifecycle-runtime-ktx:$LIFECYCLE_RUNTIME_KTX_VER"
        const val ROOM_RUNTIME = "androidx.room:room-runtime:$ROOM_VER"
        const val ROOM_COMPILER = "androidx.room:room-compiler:$ROOM_VER"
        const val ROOM_KTX = "androidx.room:room-ktx:$ROOM_VER"
    }

    object DiDeps {
        private const val DAGGER_VERSION = "2.24"

        const val DAGGER = "com.google.dagger:dagger:$DAGGER_VERSION"
        const val DAGGER_COMPILER = "com.google.dagger:dagger-compiler:$DAGGER_VERSION"
        const val DAGGER_ANDROID = "com.google.dagger:dagger-android:$DAGGER_VERSION"
        const val DAGGER_ANDROID_SUPPORT = "com.google.dagger:dagger-android-support:$DAGGER_VERSION"
        const val DAGGER_ANDROID_PROCESSOR = "com.google.dagger:dagger-android-processor:$DAGGER_VERSION"
    }

    object TestDeps {
        private const val ARCH_CORE_TESTING = "2.1.0"
        private const val CORE_LIB_VER = "1.2.0"
        private const val TEST_RUNNER_VER = "1.2.0"
        private const val TEST_RULES_VER = "1.2.0"
        private const val JUNIT_VER = "1.1.1"
        private const val TRUTH_ASSERT_VER = "1.2.0"
        private const val MOCKITO_KOTLIN_VER = "2.2.0"

        const val CORE_TESTING = "androidx.arch.core:core-testing:$ARCH_CORE_TESTING"
        const val CORE = "androidx.test:core:$CORE_LIB_VER"
        const val TEST_RUNNER = "androidx.test:runner:$TEST_RUNNER_VER"
        const val TEST_RULES = "androidx.test:rules:$TEST_RULES_VER"
        const val JUNIT = "androidx.test.ext:junit:$JUNIT_VER"
        const val TRUTH_ASSERT = "androidx.test.ext:truth:$TRUTH_ASSERT_VER"
        const val MOCKITO_KOTLIN = "com.nhaarman.mockitokotlin2:mockito-kotlin:$MOCKITO_KOTLIN_VER"
        const val COROUTINE_TEST = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${AppDeps.COROUTINES_VER }"
    }

    fun addDependencies(dependencies: DependencyHandler) {
        dependencies.implementation(AppDeps.EVENT_BUS)
        dependencies.implementation(AppDeps.APP_COMPAT)
        dependencies.implementation(AppDeps.RECYCLER_VIEW)
        dependencies.implementation(AppDeps.MATERIAL)
        dependencies.implementation(AppDeps.CONSTRAINT_LAYOUT)
        dependencies.implementation(AppDeps.KTX)
        dependencies.implementation(AppDeps.COROUTINES_CORE)
        dependencies.implementation(AppDeps.COROUTINES_ANDROID)
        dependencies.debugImplementation(AppDeps.LEAK_CANARY_ANDROID)
        dependencies.releaseImplementation(AppDeps.LEAK_CANARY_NO_OP)
        dependencies.implementation(AppDeps.TIMBER)
//        dependencies.implementation(AppDeps.RX_BINDING_CORE)
//        dependencies.implementation(AppDeps.RX_BINDING_APP_COMPAT)
        dependencies.implementation(AppDeps.CORBIND_CORE)
        dependencies.implementation(AppDeps.CORBIND_APPCOMPAT)

        dependencies.implementation(ArchCompDeps.VIEW_MODEL)
        dependencies.implementation(ArchCompDeps.VIEW_MODEL_KTX)
        dependencies.implementation(ArchCompDeps.LIVE_DATA)
        dependencies.implementation(ArchCompDeps.LIVE_DATA_KTX)
        dependencies.implementation(ArchCompDeps.LIFECYCLE_RUNTIME_KTX)
        dependencies.implementation(ArchCompDeps.ROOM_RUNTIME)
        dependencies.kapt(ArchCompDeps.ROOM_COMPILER)
        dependencies.implementation(ArchCompDeps.ROOM_KTX)

        // Dagger 2
        dependencies.implementation(DiDeps.DAGGER)
        dependencies.kapt(DiDeps.DAGGER_COMPILER)
        dependencies.implementation(DiDeps.DAGGER_ANDROID)
        dependencies.implementation(DiDeps.DAGGER_ANDROID_SUPPORT)
        dependencies.kapt(DiDeps.DAGGER_ANDROID_PROCESSOR)

        dependencies.testImplementation(TestDeps.CORE_TESTING)
        dependencies.androidTestImplementation(TestDeps.CORE_TESTING)

        dependencies.testImplementation(TestDeps.CORE)
        dependencies.androidTestImplementation(TestDeps.CORE)

        dependencies.testImplementation(TestDeps.TEST_RUNNER)
        dependencies.androidTestImplementation(TestDeps.TEST_RUNNER)

        dependencies.testImplementation(TestDeps.TEST_RULES)
        dependencies.androidTestImplementation(TestDeps.TEST_RULES)

        dependencies.testImplementation(TestDeps.JUNIT)
        dependencies.androidTestImplementation(TestDeps.JUNIT)

        dependencies.testImplementation(TestDeps.TRUTH_ASSERT)
        dependencies.androidTestImplementation(TestDeps.TRUTH_ASSERT)

        dependencies.testImplementation(TestDeps.MOCKITO_KOTLIN)
        dependencies.testImplementation(TestDeps.COROUTINE_TEST)
    }
}

fun DependencyHandler.implementation(dependency: Any) {
    add("implementation", dependency)
}

fun DependencyHandler.kapt(dependency: Any) {
    add("kapt", dependency)
}

fun DependencyHandler.debugImplementation(dependency: Any) {
    add("debugImplementation", dependency)
}

fun DependencyHandler.releaseImplementation(dependency: Any) {
    add("releaseImplementation", dependency)
}

fun DependencyHandler.testImplementation(dependency: Any) {
    add("testImplementation", dependency)
}

fun DependencyHandler.androidTestImplementation(dependency: Any) {
    add("androidTestImplementation", dependency)
}

