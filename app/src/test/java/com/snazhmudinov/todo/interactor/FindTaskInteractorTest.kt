package com.snazhmudinov.todo.interactor

import com.nhaarman.mockitokotlin2.verify
import com.snazhmudinov.todo.usecase.FindTaskUseCase
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test

class FindTaskInteractorTest : BaseInteractorTest() {

    private lateinit var findTaskInteractor: FindTaskUseCase

    @Before
    fun setup() {
        findTaskInteractor = FindTaskInteractor(repository)
    }

    @Test
    fun `find in interactor makes a corresponding call in repository`() = runBlockingTest {
        val fakeTaskId = 999L
        findTaskInteractor.find(fakeTaskId)

        verify(repository).find(fakeTaskId)
    }
}
