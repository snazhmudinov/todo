package com.snazhmudinov.todo.interactor

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.usecase.UpdateTaskUseCase
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test

class UpdateTaskInteractorTest : BaseInteractorTest() {

    private lateinit var udpdateTaskInteractor: UpdateTaskUseCase

    @Before
    fun setup() {
        udpdateTaskInteractor = UpdateTaskInteractor(repository)
    }

    @Test
    fun `update call in interactor makes a corresponding call to repository`() = runBlockingTest {
        val mockTask: Task = mock()
        udpdateTaskInteractor.update(mockTask)

        verify(repository).update(mockTask)
    }
}
