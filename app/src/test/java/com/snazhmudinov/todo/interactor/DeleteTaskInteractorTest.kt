package com.snazhmudinov.todo.interactor

import com.nhaarman.mockitokotlin2.verify
import com.snazhmudinov.todo.usecase.DeleteTaskUseCase
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test

class DeleteTaskInteractorTest :  BaseInteractorTest() {

    private lateinit var deleteTaskInteractor: DeleteTaskUseCase

    @Before
    fun setup() {
        deleteTaskInteractor = DeleteTaskInteractor(repository)
    }

    @Test
    fun `deleting all tasks in interactor makes a corresponding call in repository`() = runBlockingTest {
        deleteTaskInteractor.deleteAll()

        verify(repository).deleteAll()
    }

    @Test
    fun `deleting task by id in interactor makes a corresponding call in repository`() = runBlockingTest {
        val fakeTaskId = 999L
        deleteTaskInteractor.deleteById(fakeTaskId)

        verify(repository).deleteById(fakeTaskId)
    }
}