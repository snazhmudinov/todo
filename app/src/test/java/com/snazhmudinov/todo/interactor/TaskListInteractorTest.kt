package com.snazhmudinov.todo.interactor

import com.nhaarman.mockitokotlin2.verify
import com.snazhmudinov.todo.usecase.TasksListUseCase
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test

class TaskListInteractorTest : BaseInteractorTest() {

    private lateinit var taskListInteractor: TasksListUseCase

    @Before
    fun setup() {
        taskListInteractor = TasksListInteractor(repository)
    }

    @Test
    fun `getTasks() makes a corresponding call to the repository`() {
        taskListInteractor.getTasks()
        verify(repository).getAllAsLiveData()
    }

    @Test
    fun `getTasksAsync() makes a corresponding call to the repository`() = runBlockingTest {
        taskListInteractor.getTasksAsync()
        verify(repository).getAll()
    }
}