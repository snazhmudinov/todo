package com.snazhmudinov.todo.interactor

import com.nhaarman.mockitokotlin2.verify
import com.snazhmudinov.todo.usecase.FilterTaskUseCase
import org.junit.Before
import org.junit.Test

class FilterTaskInteractorTest : BaseInteractorTest() {

    private lateinit var filterTaskInteractor: FilterTaskUseCase

    @Before
    fun setup() {
        filterTaskInteractor = FilterTaskInteractor(repository)
    }

    @Test
    fun `filter in interactor makes a corresponding call in repository`() {
        val fakeQuery = "fakeQuery"
        filterTaskInteractor.filter(fakeQuery)

        verify(repository).filter(fakeQuery)
    }
}