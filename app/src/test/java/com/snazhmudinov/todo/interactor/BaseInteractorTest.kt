package com.snazhmudinov.todo.interactor

import com.nhaarman.mockitokotlin2.mock
import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.repository.Repository

open class BaseInteractorTest {

    protected val repository: Repository<Task> = mock()
}
