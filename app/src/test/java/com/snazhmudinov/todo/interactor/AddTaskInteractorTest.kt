package com.snazhmudinov.todo.interactor

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.usecase.AddTaskUseCase
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test

class AddTaskInteractorTest :  BaseInteractorTest() {

    private lateinit var addTaskInteractor: AddTaskUseCase

    @Before
    fun setup() {
        addTaskInteractor = AddTaskInteractor(repository)
    }

    @Test
    fun `add task in interactor calls corresponding method in repository`() = runBlockingTest {
        val mockTask: Task = mock()
        addTaskInteractor.addTask(mockTask)

        verify(repository).insert(mockTask)
    }
}