package com.snazhmudinov.todo.formatter

import com.snazhmudinov.todo.TagFormatter
import org.junit.Assert
import org.junit.Test


class TagFormatterTest {

    private val formatter = TagFormatter()

    @Test
    fun `test create tag from single string`() {
        val preTag = "android"
        val expected = formatter.createTagFrom(preTag)
        val actual = "#$preTag"

        Assert.assertEquals(expected, actual)
    }

    @Test
    fun `test create tags from string list`() {
        val list = listOf("tag1", "tag2")
        val expected = list.joinToString(separator = "") { "#$it" }
        val actual = "#tag1#tag2"

        Assert.assertEquals(expected, actual)
    }
}