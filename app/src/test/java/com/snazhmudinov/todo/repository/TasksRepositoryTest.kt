package com.snazhmudinov.todo.repository

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.dao.TasksDao
import com.snazhmudinov.todo.dao.TasksDatabase
import com.snazhmudinov.todo.repository.TasksRepository
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class TasksRepositoryTest {

    private lateinit var tasksRepository: TasksRepository
    private val db: TasksDatabase = mock()
    private val dao: TasksDao = mock()
    private val task = mock<Task>()

    @Before
    fun setup() {
        tasksRepository = TasksRepository(db)
        whenever(db.tasksDao()).thenReturn(dao)
    }

    @Test
    fun `check setup`() = Assert.assertNotNull(tasksRepository)

    @Test
    fun `fetch tasks as live data`() {
        tasksRepository.getAllAsLiveData()
        verify(dao).getAllTasks()
    }

    @Test
    fun `async fetch tasks as list`() {
        runBlocking {
            tasksRepository.getAll()
            verify(dao).getAllTasksAsync()
        }
    }

    @Test
    fun `insert a task`() {
        runBlocking {
            tasksRepository.insert(task)
            verify(dao).insertTask(task)
        }
    }

    @Test
    fun `change task status`() {
        runBlocking {
            tasksRepository.update(task)
            verify(dao).updateTask(task)
        }
    }

    @Test
    fun `delete all tasks`() {
        runBlocking {
            tasksRepository.deleteAll()
            verify(dao).deleteAllTasks()
        }
    }

    @Test
    fun `delete a single task`() {
        runBlocking {
            tasksRepository.deleteById(task.taskId)
            verify(dao).deleteTask(task.taskId)
        }
    }

    @Test
    fun `update task`() {
        runBlocking {
            tasksRepository.update(task)
            verify(dao).updateTask(task)
        }
    }

    @Test
    fun `get task by id`() {
        runBlocking {
            tasksRepository.find(task.taskId)
            verify(dao).getTaskById(task.taskId)
        }
    }

    @Test
    fun `filter tasks`() {
        val query = "query"
        tasksRepository.filter(query)
        verify(dao).filterTasksByTag("%$query%")
    }
}