package com.snazhmudinov.todo.viewmodel

import androidx.databinding.PropertyChangeRegistry
import androidx.databinding.library.baseAdapters.BR
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.snazhmudinov.todo.CoroutinesTestRule
import com.snazhmudinov.todo.interactor.AddTaskInteractor
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class AddTaskViewModelTest {

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    private lateinit var addTaskViewModel: AddTaskViewModel

    private val addTaskInteractor: AddTaskInteractor = mock()
    private var callbackMock: PropertyChangeRegistry = mock()

    @Before
    fun setup() {
        addTaskViewModel =
            AddTaskViewModel(coroutinesTestRule.testDispatcher, addTaskInteractor, callbackMock)
    }

    @Test
    fun `setup done`() {
        Assert.assertNotNull(addTaskViewModel)
    }

    @Test
    fun `test trigger observer`() {
        addTaskViewModel.taskTitle = "task title"
        verify(callbackMock).notifyCallbacks(addTaskViewModel, BR.taskTitle, null)

        addTaskViewModel.taskDescription = "task description"
        verify(callbackMock).notifyCallbacks(addTaskViewModel, BR.taskDescription, null)
    }

    @Test
    fun `test insert new task`() = runBlockingTest {
        addTaskViewModel.insertTask()
        verify(addTaskInteractor).addTask(any())
    }
}