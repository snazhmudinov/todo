package com.snazhmudinov.todo.viewmodel

import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.snazhmudinov.todo.CoroutinesTestRule
import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.interactor.FindTaskInteractor
import com.snazhmudinov.todo.interactor.UpdateTaskInteractor
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class EditTaskViewModelTest {

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    private lateinit var editTaskViewModel: EditTaskViewModel

    private val updateTaskInteractor: UpdateTaskInteractor = mock()
    private val findTaskInteractor: FindTaskInteractor = mock()
    private val task: MutableLiveData<Task> = mock()

    @Before
    fun setup() {
        editTaskViewModel = EditTaskViewModel(
            coroutinesTestRule.testDispatcher,
            findTaskInteractor,
            updateTaskInteractor,
            task
        )
    }

    @Test
    fun `setup done`() {
        Assert.assertNotNull(editTaskViewModel)
    }

    @Test
    fun `setting task id fetches the task and notifies observer`() = runBlockingTest {
        val result: Task = mock()
        whenever(findTaskInteractor.find(0L)).doReturn(result)

        editTaskViewModel.setTaskId(0L)

        verify(findTaskInteractor).find(0L)
        verify(task).postValue(result)
    }

    @Test
    fun `update task makes repository call`() = runBlockingTest {
        val result: Task = mock()
        whenever(task.value).doReturn(result)

        editTaskViewModel.updateTask()
        verify(updateTaskInteractor).update(result)
    }
}