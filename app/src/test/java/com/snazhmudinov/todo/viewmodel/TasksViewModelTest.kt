package com.snazhmudinov.todo.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.snazhmudinov.todo.CoroutinesTestRule
import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.interactor.DeleteTaskInteractor
import com.snazhmudinov.todo.interactor.FilterTaskInteractor
import com.snazhmudinov.todo.interactor.TasksListInteractor
import com.snazhmudinov.todo.interactor.UpdateTaskInteractor
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class TasksViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    private lateinit var tasksViewModel: TasksViewModel

    private val updateTaskInteractor: UpdateTaskInteractor = mock()
    private val tasksListInteractor: TasksListInteractor = mock()
    private val filterTaskInteractor: FilterTaskInteractor = mock()
    private val deleteTaskInteractor: DeleteTaskInteractor = mock()
    private val mockLiveData: MediatorLiveData<List<Task>> = mock()
    private val tasksObserver: Observer<List<Task>> = mock()

    @Before
    fun setup() {
        tasksViewModel = TasksViewModel(
            coroutinesTestRule.testDispatcher,
            updateTaskInteractor,
            tasksListInteractor,
            filterTaskInteractor,
            deleteTaskInteractor
        )
        whenever(tasksViewModel.tasks).doReturn(mockLiveData)
        tasksViewModel.tasks.observeForever(tasksObserver)
    }

    @After
    fun tearDown() {
        tasksViewModel.tasks.removeObserver(tasksObserver)
    }

    @Test
    fun `setup is done`() {
        Assert.assertNotNull(tasksViewModel)
    }

    @Test
    fun `delete all tasks`() = runBlocking {
        tasksViewModel.deleteAllTasks()
        verify(deleteTaskInteractor).deleteAll()
    }

    @Test
    fun `delete single task`() = runBlocking {
        val id = 1L
        tasksViewModel.deleteTask(id)

        verify(deleteTaskInteractor).deleteById(id)
    }

    @Test
    fun `test query tasks`() {
        val mediatorLiveData = tasksViewModel.tasks

        whenever(filterTaskInteractor.filter("task")).doAnswer {
            mediatorLiveData.postValue(any())
            mock()
        }

        whenever(tasksListInteractor.getTasks()).doAnswer {
            mediatorLiveData.postValue(any())
            mock()
        }

        tasksViewModel.queryTasks("task")
        verify(filterTaskInteractor, atLeastOnce()).filter("task")

        tasksViewModel.queryTasks("")
        verify(tasksListInteractor, atLeastOnce()).getTasks()

        verify(tasksObserver, times(2)).onChanged(null)
    }
}