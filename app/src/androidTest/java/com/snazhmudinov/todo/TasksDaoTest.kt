package com.snazhmudinov.todo

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.dao.TasksDao
import com.snazhmudinov.todo.dao.TasksDatabase
import kotlinx.coroutines.runBlocking
import org.hamcrest.core.IsEqual.equalTo
import org.junit.After
import org.junit.Assert.assertNull
import org.junit.Assert.assertThat
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TasksDaoTest {

    private lateinit var tasksDao: TasksDao
    private lateinit var tasksDb: TasksDatabase
    private val task = Task(taskId = 1L, title = "Test task", details = "Details", done = false, tags = emptyList())

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        tasksDb = Room.inMemoryDatabaseBuilder(context, TasksDatabase::class.java).build()
        tasksDao = tasksDb.tasksDao()
    }

    @After
    fun tearDown() {
        tasksDb.close()
    }

    @Test
    fun testInsertTask() = runBlocking {
        tasksDao.insertTask(task)

        val actualTask = tasksDao.getTaskById(1L)
        assertThat(actualTask.taskId, equalTo(task.taskId))
    }

    @Test
    fun testDeleteTask() = runBlocking {
        tasksDao.insertTask(task)
        tasksDao.deleteTask(1L)

        val actualTask = tasksDao.getTaskById(1L)
        assertNull(actualTask)
    }

    @Test
    fun testDeleteAllTasks() = runBlocking {
        val tasks = listOf(
                Task(title = "Test task 1", details = "Details", done = false, tags = emptyList()),
                Task(title = "Test task 2", details = "Details", done = false, tags = emptyList())
        )

        tasks.forEach { tasksDao.insertTask(it) }

        tasksDao.deleteAllTasks()

        val dbTasks = LiveDataTestUtil.getValue(tasksDao.getAllTasks())
        assertThat(dbTasks.size, equalTo(0))
    }

    @Test
    fun testUpdateTask() = runBlocking {
        val newTaskTitle = "Updated task title"

        tasksDao.insertTask(task)

        task.title = newTaskTitle

        tasksDao.updateTask(task)

        val updatedTask = tasksDao.getTaskById(1L)

        assertThat(updatedTask.title, equalTo(newTaskTitle))
    }

    @Test
    fun testSetTaskStatus() = runBlocking {
        tasksDao.insertTask(task)
        tasksDao.setTaskStatus(task.taskId, done = true)

        val updatedTask = tasksDao.getTaskById(1L)
        assertThat(updatedTask.done, equalTo(true))
    }

    @Test
    fun testFilterNoResult() = runBlocking {
        val filteredTasks = LiveDataTestUtil.getValue(tasksDao.filterTasksByTag("%dummy-query%"))
        assertTrue(filteredTasks.isEmpty())
    }

    @Test
    fun testFilterFoundMatch() = runBlocking {
        val tasks = listOf(
            Task(title = "Test task 1", details = "Details", done = false, tags = listOf("tag1")),
            Task(title = "Test task 2", details = "Details", done = false, tags = listOf("tag2"))
        )

        tasks.forEach {
            tasksDao.insertTask(it)
        }

        val filteredTasks = LiveDataTestUtil.getValue(tasksDao.filterTasksByTag("%tag1%"))

        assertTrue(filteredTasks.isNotEmpty())
    }
}
