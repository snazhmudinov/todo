package com.snazhmudinov.todo.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Update

@Dao
interface TasksDao {

    @Query("SELECT * FROM tasks")
    fun getAllTasks(): LiveData<List<Task>>

    @Query("SELECT * FROM tasks")
    suspend fun getAllTasksAsync(): List<Task>

    @Insert(onConflict = REPLACE)
    suspend fun insertTask(task: Task)

    @Query("DELETE FROM tasks WHERE taskId = :taskId")
    suspend fun deleteTask(taskId: Long)

    @Query("DELETE FROM tasks")
    suspend fun deleteAllTasks()

    @Update(onConflict = REPLACE)
    suspend fun updateTask(task: Task)

    @Query("SELECT * FROM tasks WHERE taskId = :taskId")
    suspend fun getTaskById(taskId: Long): Task

    @Query("UPDATE tasks SET done = :done WHERE taskId = :taskId")
    suspend fun setTaskStatus(taskId: Long, done: Boolean)

    @Query("SELECT * FROM tasks WHERE tags LIKE :query")
    fun filterTasksByTag(query: String): LiveData<List<Task>>

    @Query("SELECT * FROM tasks WHERE taskId = :id")
    suspend fun getTaskWithId(id: Long): Task?
}