package com.snazhmudinov.todo.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.snazhmudinov.todo.adapter.TagConverter

@Database(entities = [Task::class], version = 2, exportSchema = false)
@TypeConverters(TagConverter::class)
abstract class TasksDatabase : RoomDatabase() {

    abstract fun tasksDao(): TasksDao
}