package com.snazhmudinov.todo.dao

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "tasks", indices = [Index("tags")])
data class Task(@PrimaryKey(autoGenerate = true) val taskId: Long = 0L,
                @ColumnInfo var title: String,
                @ColumnInfo var details: String,
                @ColumnInfo var done: Boolean,
                @ColumnInfo var tags: List<String>)