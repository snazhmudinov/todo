package com.snazhmudinov.todo.usecase

interface DeleteTaskUseCase {

    suspend fun deleteAll()

    suspend fun deleteById(taskId: Long)
}
