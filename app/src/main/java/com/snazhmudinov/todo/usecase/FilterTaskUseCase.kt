package com.snazhmudinov.todo.usecase

import androidx.lifecycle.LiveData
import com.snazhmudinov.todo.dao.Task

interface FilterTaskUseCase {

    fun filter(query: String): LiveData<List<Task>>
}
