package com.snazhmudinov.todo.usecase

import com.snazhmudinov.todo.dao.Task

interface UpdateTaskUseCase {

    suspend fun update(task: Task)
}
