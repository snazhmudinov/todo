package com.snazhmudinov.todo.usecase

import androidx.lifecycle.LiveData
import com.snazhmudinov.todo.dao.Task

interface TasksListUseCase {

    fun getTasks(): LiveData<List<Task>>

    suspend fun getTasksAsync(): List<Task>
}
