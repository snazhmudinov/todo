package com.snazhmudinov.todo.usecase

import com.snazhmudinov.todo.dao.Task

interface AddTaskUseCase {

    suspend fun addTask(task: Task)
}
