package com.snazhmudinov.todo.usecase

import com.snazhmudinov.todo.dao.Task

interface FindTaskUseCase {

    suspend fun find(taskId: Long): Task
}
