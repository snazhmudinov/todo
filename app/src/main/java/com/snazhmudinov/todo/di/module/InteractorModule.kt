package com.snazhmudinov.todo.di.module

import com.snazhmudinov.todo.interactor.AddTaskInteractor
import com.snazhmudinov.todo.interactor.DeleteTaskInteractor
import com.snazhmudinov.todo.interactor.FilterTaskInteractor
import com.snazhmudinov.todo.interactor.FindTaskInteractor
import com.snazhmudinov.todo.interactor.TasksListInteractor
import com.snazhmudinov.todo.interactor.UpdateTaskInteractor
import com.snazhmudinov.todo.usecase.AddTaskUseCase
import com.snazhmudinov.todo.usecase.DeleteTaskUseCase
import com.snazhmudinov.todo.usecase.FilterTaskUseCase
import com.snazhmudinov.todo.usecase.FindTaskUseCase
import com.snazhmudinov.todo.usecase.TasksListUseCase
import com.snazhmudinov.todo.usecase.UpdateTaskUseCase
import dagger.Binds
import dagger.Module

@Module
abstract class InteractorModule {

    @Binds
    abstract fun bindAddTaskUseCase(interactor: AddTaskInteractor): AddTaskUseCase

    @Binds
    abstract fun bindDeleteTaskUseCase(interactor: DeleteTaskInteractor): DeleteTaskUseCase

    @Binds
    abstract fun bindFilterTaskUseCase(interactor: FilterTaskInteractor): FilterTaskUseCase

    @Binds
    abstract fun bindFindTaskUseCase(interactor: FindTaskInteractor): FindTaskUseCase

    @Binds
    abstract fun bindTasksListUseCase(interactor: TasksListInteractor): TasksListUseCase

    @Binds
    abstract fun bindUpdateTaskUseCase(interactor: UpdateTaskInteractor): UpdateTaskUseCase
}
