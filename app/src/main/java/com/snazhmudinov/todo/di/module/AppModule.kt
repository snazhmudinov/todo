package com.snazhmudinov.todo.di.module

import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.snazhmudinov.todo.BuildConfig
import com.snazhmudinov.todo.dao.Task
import dagger.Module
import dagger.Provides
import org.greenrobot.eventbus.EventBus
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideBus(): EventBus = EventBus.builder().throwSubscriberException(BuildConfig.DEBUG).installDefaultEventBus()

    @Provides
    fun providesPropertyChangeRegistry(): PropertyChangeRegistry = PropertyChangeRegistry()

    @Provides
    fun providesLiveDataTask(): MutableLiveData<Task> = MutableLiveData<Task>()
}