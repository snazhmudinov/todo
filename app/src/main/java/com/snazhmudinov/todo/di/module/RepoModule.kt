package com.snazhmudinov.todo.di.module

import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.repository.Repository
import com.snazhmudinov.todo.repository.TasksRepository
import dagger.Binds
import dagger.Module

@Module
abstract class RepoModule {

    @Binds
    abstract fun bindsRepository(tasksRepository: TasksRepository): Repository<Task>
}