package com.snazhmudinov.todo.di.module

import com.snazhmudinov.todo.activity.AddTaskActivity
import com.snazhmudinov.todo.activity.EditTaskActivity
import com.snazhmudinov.todo.activity.TasksListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun tasksActivity(): TasksListActivity

    @ContributesAndroidInjector
    abstract fun editTaskActivity(): EditTaskActivity

    @ContributesAndroidInjector
    abstract fun addTaskActivity(): AddTaskActivity
}