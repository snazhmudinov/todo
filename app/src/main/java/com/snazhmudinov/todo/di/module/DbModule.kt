package com.snazhmudinov.todo.di.module

import android.content.Context
import androidx.room.Room
import com.snazhmudinov.todo.dao.TasksDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule {

    @Provides
    @Singleton
    fun provideDb(context: Context): TasksDatabase = Room.databaseBuilder(context, TasksDatabase::class.java, "tasks.db").build()
}