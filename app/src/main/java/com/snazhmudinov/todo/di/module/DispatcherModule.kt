package com.snazhmudinov.todo.di.module

import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Named

@Module
class DispatcherModule {

    @Provides
    @Named("IO")
    fun providesIO(): CoroutineDispatcher = Dispatchers.IO
}