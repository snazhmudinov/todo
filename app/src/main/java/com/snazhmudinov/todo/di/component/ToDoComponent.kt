package com.snazhmudinov.todo.di.component

import android.content.Context
import com.snazhmudinov.todo.application.ToDoApplication
import com.snazhmudinov.todo.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ViewModelModule::class,
    AppModule::class,
    DbModule::class,
    RepoModule::class,
    ActivityModule::class,
    ServiceModule::class,
    DispatcherModule::class,
    InteractorModule::class
])
interface ToDoComponent : AndroidInjector<ToDoApplication> {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance appContext: Context): ToDoComponent
    }
}