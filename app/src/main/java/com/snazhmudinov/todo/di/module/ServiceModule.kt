package com.snazhmudinov.todo.di.module

import com.snazhmudinov.todo.ListViewWidgetService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServiceModule {
    @ContributesAndroidInjector
    abstract fun widget(): ListViewWidgetService
}