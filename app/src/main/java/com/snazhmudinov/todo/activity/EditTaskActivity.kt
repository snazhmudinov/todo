package com.snazhmudinov.todo.activity

import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.snazhmudinov.todo.R
import com.snazhmudinov.todo.ToDoWidget
import com.snazhmudinov.todo.databinding.ActivityTaskDetailsBinding
import com.snazhmudinov.todo.viewmodel.EditTaskViewModel
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_task_details.*
import javax.inject.Inject

class EditTaskActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private lateinit var editTaskViewModel: EditTaskViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(taskDetailsToolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        editTaskViewModel = ViewModelProviders.of(this, factory).get(EditTaskViewModel::class.java)

        val id = intent.getLongExtra(TasksListActivity.TASK_ID_EXTRA, -1L)
        val isWidgetCall = intent.getBooleanExtra(ToDoWidget.WIDGET_CALL_EXTRA, false)

        if (id == -1L) {
            finish()
            return
        }

        val binding = DataBindingUtil.setContentView<ActivityTaskDetailsBinding>(this, R.layout.activity_task_details)
        binding.lifecycleOwner = this
        binding.viewModel = editTaskViewModel

        editTaskViewModel.setTaskId(id)

        taskDetailsToolbar.setNavigationOnClickListener {
            // Update task & close
            editTaskViewModel.updateTask()

            // If call made via widget update widget
            if (isWidgetCall) {
                val manager = AppWidgetManager.getInstance(this)
                val ids = manager.getAppWidgetIds(ComponentName(this, ToDoWidget::class.java))

                val updateIntent = Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE).apply {
                    putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
                    putExtra(TasksListActivity.EMPTY_EXTRA_KEY, false)
                }

                sendBroadcast(updateIntent)
            }

            finish()
        }
    }
}
