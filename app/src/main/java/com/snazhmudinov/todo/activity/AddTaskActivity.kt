package com.snazhmudinov.todo.activity

import android.os.Bundle
import android.view.inputmethod.EditorInfo
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.snazhmudinov.todo.R
import com.snazhmudinov.todo.databinding.ActivityAddTaskBinding
import com.snazhmudinov.todo.viewmodel.AddTaskViewModel
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_add_task.*
import javax.inject.Inject

class AddTaskActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private lateinit var addTaskViewModel: AddTaskViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        addTaskViewModel = ViewModelProviders.of(this, factory).get(AddTaskViewModel::class.java)

        val binding = DataBindingUtil.setContentView<ActivityAddTaskBinding>(this, R.layout.activity_add_task)
        binding.lifecycleOwner = this
        binding.addTaskViewModel = addTaskViewModel

        setSupportActionBar(addTaskToolbar)

        createTask.setOnClickListener {
            addTaskViewModel.insertTask()
            finish()
        }

        cancel.setOnClickListener { finish() }

        /*
        Tag input handling
         */
        tagField.setOnEditorActionListener { v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val tagText = v.text.toString().trim()

                if (tagText.isNotEmpty()) {
                    createTagView(tagText)
                    addTag(tagText)
                }

                tagField.text = null
            }
            false
        }

        addTaskViewModel.tags.forEach {
            createTagView(it)
        }
    }

    private fun addTag(s: String) {
        addTaskViewModel.tags += s
    }

    private fun createTagView(s: String) {
        with(Chip(this)) {
            text = s
            isCloseIconVisible = true

            layoutParams = ChipGroup.LayoutParams(ChipGroup.LayoutParams.WRAP_CONTENT,
                    resources.getDimensionPixelSize(R.dimen.chip_height))

            setOnCloseIconClickListener {
                chipGroup.removeView(it)
                addTaskViewModel.tags.remove(s)
            }

            chipGroup.addView(this, chipGroup.childCount - 1)
        }
    }
}