package com.snazhmudinov.todo.activity

import android.app.SearchManager
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.SearchView
import androidx.core.content.getSystemService
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import com.snazhmudinov.todo.R
import com.snazhmudinov.todo.TagFormatter
import com.snazhmudinov.todo.ToDoWidget
import com.snazhmudinov.todo.adapter.TasksAdapter
import com.snazhmudinov.todo.callback.SwipeDeleteInterface
import com.snazhmudinov.todo.callback.SwipeToDeleteCallback
import com.snazhmudinov.todo.event.Bus
import com.snazhmudinov.todo.event.TaskSelectedEvent
import com.snazhmudinov.todo.event.TaskStatusChangedEvent
import com.snazhmudinov.todo.viewmodel.TasksViewModel
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_tasks_list.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import ru.ldralighieri.corbind.appcompat.queryTextChangeEvents
import javax.inject.Inject

class TasksListActivity : DaggerAppCompatActivity(), SwipeDeleteInterface {

    companion object {
        const val TASK_ID_EXTRA = "TASK_ID_EXTRA"
        const val EMPTY_EXTRA_KEY = "EMPTY_EXTRA_KEY"
    }

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    @Inject
    lateinit var tagFormatError: TagFormatter

    @Inject
    lateinit var eventBus: EventBus

    private lateinit var bus: Bus

    private lateinit var tasksViewModel: TasksViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        bus = Bus(eventBus, this)

        setContentView(R.layout.activity_tasks_list)
        setSupportActionBar(tasksToolbar)

        tasksViewModel = ViewModelProviders.of(this, factory).get(TasksViewModel::class.java)

        val tasksAdapter = TasksAdapter(this, bus, tagFormatError, lifecycleScope)

        with(tasksRecyclerView) {
            adapter = tasksAdapter
            addItemDecoration(DividerItemDecoration(this@TasksListActivity, DividerItemDecoration.VERTICAL))
        }

        val swipeHelper = ItemTouchHelper(SwipeToDeleteCallback(this, this))
        swipeHelper.attachToRecyclerView(tasksRecyclerView)

        tasksViewModel.tasks.observe(this, Observer { list ->
            tasksAdapter.submitList(list)
            updateWidget(list.isEmpty())

            emptyListTextView.isVisible = list.isEmpty()
            tasksRecyclerView.isGone = list.isEmpty()
        })

        addTask.setOnClickListener {
            startActivity(Intent(this, AddTaskActivity::class.java))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)

        // Associate searchable configuration with the SearchView
        val searchManager = getSystemService<SearchManager>()
        (menu?.findItem(R.id.search)?.actionView as SearchView).apply {
            setSearchableInfo(searchManager?.getSearchableInfo(componentName))

            lifecycleScope.launch(Dispatchers.Main) {
                queryTextChangeEvents().debounce(300).collect {
                    tasksViewModel.queryTasks(it.queryText.toString())
                }
            }
        }

        menu.findItem(R.id.search).setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                addTask.isVisible = false
                menu.findItem(R.id.deleteAllTasks).isVisible = false
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                tasksViewModel.queryTasks("")

                addTask.isVisible = true
                menu.findItem(R.id.deleteAllTasks).isVisible = true
                invalidateOptionsMenu()

                return true
            }

        })

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.deleteAllTasks -> {
                tasksViewModel.deleteAllTasks()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (Intent.ACTION_SEARCH == intent?.action) {
            val query = intent.getStringExtra(SearchManager.QUERY)
            tasksViewModel.queryTasks(query)
        }
    }

    private fun updateWidget(empty: Boolean) {
        val manager = AppWidgetManager.getInstance(this)
        val ids = manager.getAppWidgetIds(ComponentName(this, ToDoWidget::class.java))

        val updateIntent = Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE).apply {
            putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
            putExtra(EMPTY_EXTRA_KEY, empty)
        }

        sendBroadcast(updateIntent)
    }

    override fun onDeleteTask(taskId: Long) {
        tasksViewModel.deleteTask(taskId)
    }

    @Suppress("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onTaskSelected(event: TaskSelectedEvent) {
        val intent = Intent(this, EditTaskActivity::class.java).apply {
            putExtra(TASK_ID_EXTRA, event.taskId)
        }

        startActivity(intent)
    }

    @Suppress("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onTaskStateChanged(event: TaskStatusChangedEvent) {
        tasksViewModel.changeTaskStatus(event.task)
    }
}
