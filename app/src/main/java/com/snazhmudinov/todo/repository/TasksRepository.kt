package com.snazhmudinov.todo.repository

import androidx.lifecycle.LiveData
import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.dao.TasksDatabase
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TasksRepository @Inject constructor(private val tasksDatabase: TasksDatabase) :
    Repository<Task> {

    override fun getAllAsLiveData(): LiveData<List<Task>> =
        tasksDatabase.tasksDao().getAllTasks()

    override suspend fun getAll(): List<Task> = tasksDatabase.tasksDao().getAllTasksAsync()

    override suspend fun find(id: Long): Task = tasksDatabase.tasksDao().getTaskById(id)

    override suspend fun insert(e: Task) = tasksDatabase.tasksDao().insertTask(e)

    override suspend fun deleteById(id: Long) = tasksDatabase.tasksDao().deleteTask(id)

    override suspend fun deleteAll() = tasksDatabase.tasksDao().deleteAllTasks()

    override suspend fun update(e: Task) = tasksDatabase.tasksDao().updateTask(e)

    override fun filter(q: String): LiveData<List<Task>> =
        tasksDatabase.tasksDao().filterTasksByTag("%$q%")
}

interface Repository<T> {

    fun getAllAsLiveData(): LiveData<List<T>>

    suspend fun getAll(): List<T>

    suspend fun find(id: Long): T

    suspend fun insert(e: T)

    suspend fun deleteById(id: Long)

    suspend fun deleteAll()

    suspend fun update(e: T)

    fun filter(q: String): LiveData<List<T>>
}