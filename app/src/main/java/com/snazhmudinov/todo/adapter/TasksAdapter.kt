package com.snazhmudinov.todo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.snazhmudinov.todo.TagFormatter
import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.databinding.TaskViewHolderBinding
import com.snazhmudinov.todo.event.Bus
import com.snazhmudinov.todo.event.TaskSelectedEvent
import com.snazhmudinov.todo.event.TaskStatusChangedEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.launch
import ru.ldralighieri.corbind.view.clicks

class TasksAdapter(context: Context,
                   private val bus: Bus,
                   private val tagFormatter: TagFormatter,
                   private val scope: CoroutineScope) :
        DataBoundAdapter<Task, TaskViewHolderBinding>(context, TaskDiffCallback()) {

    init {
        setHasStableIds(true)
    }

    override fun create(layoutInflater: LayoutInflater, parent: ViewGroup): DataBoundViewHolder<TaskViewHolderBinding> {
        val binding = TaskViewHolderBinding.inflate(layoutInflater, parent, false)
        return TaskViewHolder(binding)
    }

    override fun bind(item: Task, holder: DataBoundViewHolder<TaskViewHolderBinding>) {
        holder.binding.task = item
        holder.binding.tagFormatter = tagFormatter
        scope.launch(Dispatchers.Main) {
            holder.binding.taskStatus.clicks().debounce(200).collect {
                bus.post(TaskStatusChangedEvent(item))
            }
        }
    }

    override fun onViewHolderClick(item: Task) = bus.post(TaskSelectedEvent(item.taskId))

    override fun getItemId(position: Int): Long = getItem(position).taskId
}

class TaskViewHolder(binding: TaskViewHolderBinding) : DataBoundViewHolder<TaskViewHolderBinding>(binding)

class TaskDiffCallback : DiffUtil.ItemCallback<Task>() {

    override fun areItemsTheSame(oldItem: Task, newItem: Task) = oldItem.taskId == newItem.taskId

    override fun areContentsTheSame(oldItem: Task, newItem: Task) = oldItem == newItem
}