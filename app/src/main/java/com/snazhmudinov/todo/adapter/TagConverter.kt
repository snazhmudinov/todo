package com.snazhmudinov.todo.adapter

import androidx.room.TypeConverter

class TagConverter {

    @TypeConverter
    fun toList(tags: String): List<String> {
        return if (tags.isNotEmpty()) tags.split(";") else emptyList()
    }

    @TypeConverter
    fun fromList(tags: List<String>): String {
        return if (tags.isNotEmpty()) tags.joinToString(";") else ""
    }
}