package com.snazhmudinov.todo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

abstract class DataBoundAdapter<T, V : ViewDataBinding>(context: Context,
                                                        diffCallback: DiffUtil.ItemCallback<T>) :
        ListAdapter<T, DataBoundViewHolder<V>>(diffCallback) {

    private val layoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBoundViewHolder<V> {
        return create(layoutInflater, parent)
    }

    override fun onBindViewHolder(holder: DataBoundViewHolder<V>, position: Int) {
        val item = getItem(position)
        holder.binding.root.setOnClickListener {
            onViewHolderClick(item)
        }
        bind(item, holder)
    }

    abstract fun create(layoutInflater: LayoutInflater, parent: ViewGroup): DataBoundViewHolder<V>

    abstract fun bind(item: T, holder: DataBoundViewHolder<V>)

    open protected fun onViewHolderClick(item: T) {}
}

open class DataBoundViewHolder<T : ViewDataBinding>(val binding: T) : RecyclerView.ViewHolder(binding.root)