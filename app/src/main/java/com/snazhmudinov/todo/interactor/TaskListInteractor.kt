package com.snazhmudinov.todo.interactor

import androidx.lifecycle.LiveData
import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.repository.Repository
import com.snazhmudinov.todo.usecase.TasksListUseCase
import javax.inject.Inject

class TasksListInteractor @Inject constructor(
    private val repository: Repository<Task>
) : TasksListUseCase {

    override fun getTasks(): LiveData<List<Task>> = repository.getAllAsLiveData()

    override suspend fun getTasksAsync(): List<Task> = repository.getAll()
}
