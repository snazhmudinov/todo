package com.snazhmudinov.todo.interactor

import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.repository.Repository
import com.snazhmudinov.todo.usecase.FindTaskUseCase
import javax.inject.Inject

class FindTaskInteractor @Inject constructor(
    private val repository: Repository<Task>
) : FindTaskUseCase {

    override suspend fun find(taskId: Long): Task = repository.find(taskId)
}
