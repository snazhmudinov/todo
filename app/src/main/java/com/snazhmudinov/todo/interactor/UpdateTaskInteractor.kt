package com.snazhmudinov.todo.interactor

import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.repository.Repository
import com.snazhmudinov.todo.usecase.UpdateTaskUseCase
import javax.inject.Inject

class UpdateTaskInteractor @Inject constructor(
    private val repository: Repository<Task>
) : UpdateTaskUseCase {

    override suspend fun update(task: Task) {
        repository.update(task)
    }
}
