package com.snazhmudinov.todo.interactor

import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.repository.Repository
import com.snazhmudinov.todo.usecase.AddTaskUseCase
import javax.inject.Inject

class AddTaskInteractor @Inject constructor(
    private var repository: Repository<Task>
) : AddTaskUseCase {

    override suspend fun addTask(task: Task) {
        repository.insert(task)
    }
}
