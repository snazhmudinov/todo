package com.snazhmudinov.todo.interactor

import androidx.lifecycle.LiveData
import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.repository.Repository
import com.snazhmudinov.todo.usecase.FilterTaskUseCase
import javax.inject.Inject

class FilterTaskInteractor @Inject constructor(
    private val repository: Repository<Task>
) : FilterTaskUseCase {

    override fun filter(query: String): LiveData<List<Task>> = repository.filter(query)
}
