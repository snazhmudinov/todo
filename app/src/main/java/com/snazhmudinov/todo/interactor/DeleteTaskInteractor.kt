package com.snazhmudinov.todo.interactor

import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.repository.Repository
import com.snazhmudinov.todo.usecase.DeleteTaskUseCase
import javax.inject.Inject

class DeleteTaskInteractor @Inject constructor(
    private val repository: Repository<Task>
) : DeleteTaskUseCase {

    override suspend fun deleteAll() = repository.deleteAll()

    override suspend fun deleteById(taskId: Long) = repository.deleteById(taskId)
}
