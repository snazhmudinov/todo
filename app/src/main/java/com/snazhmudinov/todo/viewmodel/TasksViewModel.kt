package com.snazhmudinov.todo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.usecase.DeleteTaskUseCase
import com.snazhmudinov.todo.usecase.FilterTaskUseCase
import com.snazhmudinov.todo.usecase.TasksListUseCase
import com.snazhmudinov.todo.usecase.UpdateTaskUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named

class TasksViewModel @Inject constructor(
    @Named("IO") private val io: CoroutineDispatcher,
    private val updateTaskUseCase: UpdateTaskUseCase,
    private val tasksListUseCase: TasksListUseCase,
    private val filterTaskUseCase: FilterTaskUseCase,
    private val deleteTaskUseCase: DeleteTaskUseCase
) : ViewModel() {

    val tasks = MediatorLiveData<List<Task>>()

    private var removeLiveData: LiveData<List<Task>>? = null

    init {
        addSource(tasksListUseCase.getTasks())
    }

    fun deleteAllTasks() {
        viewModelScope.launch(io) {
            deleteTaskUseCase.deleteAll()
        }
    }

    fun deleteTask(taskId: Long) {
        viewModelScope.launch(io) {
            deleteTaskUseCase.deleteById(taskId)
        }
    }

    fun queryTasks(query: String) {
        addSource(
            if (query.isEmpty()) {
                tasksListUseCase.getTasks()
            } else {
                filterTaskUseCase.filter(query)
            }
        )
    }

    // TODO: Add test case?
    private fun addSource(source: LiveData<List<Task>>) {
        removeLiveData?.let {
            tasks.removeSource(it)
        }

        removeLiveData = source
        removeLiveData?.let {
            tasks.addSource(source) { list ->
                tasks.postValue(list)
            }
        }
    }

    fun changeTaskStatus(task: Task) {
        viewModelScope.launch(io) {
            updateTaskUseCase.update(task)
        }
    }
}