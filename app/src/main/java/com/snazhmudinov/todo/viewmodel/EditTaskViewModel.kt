package com.snazhmudinov.todo.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.interactor.UpdateTaskInteractor
import com.snazhmudinov.todo.usecase.FindTaskUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named

class EditTaskViewModel @Inject constructor(
    @Named("IO") private val io: CoroutineDispatcher,
    private val findTaskUseCase: FindTaskUseCase,
    private val updateTaskInteractor: UpdateTaskInteractor,
    val task: MutableLiveData<Task>
) : ViewModel() {

    fun setTaskId(taskId: Long) {
        viewModelScope.launch(io) {
            val t = findTaskUseCase.find(taskId)
            task.postValue(t)
        }
    }

    fun updateTask() {
        task.value?.let {
            viewModelScope.launch(io) {
                updateTaskInteractor.update(it)
            }
        }
    }
}