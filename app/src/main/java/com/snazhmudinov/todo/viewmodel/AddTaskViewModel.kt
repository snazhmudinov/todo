package com.snazhmudinov.todo.viewmodel

import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.usecase.AddTaskUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named
import kotlin.properties.Delegates
import kotlin.reflect.KProperty

class AddTaskViewModel @Inject constructor(
    @Named("IO") private val io: CoroutineDispatcher,
    private val addTaskUseCase: AddTaskUseCase,
    private val callbacks: PropertyChangeRegistry
) : ViewModel(), Observable {

    @get:Bindable
    var taskTitle: String by Delegates.observable("") { _: KProperty<*>, _: String, _: String ->
        notifyPropertyChanged(BR.taskTitle)
    }

    @get:Bindable
    var taskDescription: String by Delegates.observable("") { _: KProperty<*>, _: String, _: String ->
        notifyPropertyChanged(BR.taskDescription)
    }

    val tags = mutableListOf<String>()

    fun insertTask() {
        viewModelScope.launch(io) {
            addTaskUseCase.addTask(
                Task(
                    title = taskTitle,
                    details = taskDescription,
                    done = false,
                    tags = tags
                )
            )
        }
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.remove(callback)
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.add(callback)
    }

    fun notifyPropertyChanged(fieldId: Int) {
        callbacks.notifyCallbacks(this, fieldId, null)
    }
}