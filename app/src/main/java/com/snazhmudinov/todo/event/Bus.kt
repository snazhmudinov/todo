package com.snazhmudinov.todo.event

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import org.greenrobot.eventbus.EventBus
import timber.log.Timber
import javax.inject.Inject

class Bus @Inject constructor(
        private val eventBus: EventBus,
        private val lifecycleOwner: LifecycleOwner) : LifecycleObserver {

    init {
        lifecycleOwner.lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun register() {
        Timber.i("Registering bus")
        eventBus.register(lifecycleOwner)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun unregister() {
        Timber.i("Unregistering bus")
        eventBus.unregister(lifecycleOwner)
    }

    fun post(event: Any) {
        eventBus.post(event)
    }
}