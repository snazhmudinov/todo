package com.snazhmudinov.todo.event

class TaskSelectedEvent(val taskId: Long)