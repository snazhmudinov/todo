package com.snazhmudinov.todo.event

import com.snazhmudinov.todo.dao.Task

class TaskStatusChangedEvent(val task: Task)