package com.snazhmudinov.todo

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TagFormatter @Inject constructor() {

    /*
     Formats the given string into tag, e.g. android -> #android
    */
    fun createTagFrom(str: String): String = "#${str.trim()}"

    /*
     Formats the list of strings into tags, e.g. [android, kotlin] -> #android#kotlin
     */
    fun createTags(tags: List<String>): String {
        return if (tags.isNotEmpty()) {
            tags.joinToString("") { createTagFrom(it) }
        } else ""
    }
}