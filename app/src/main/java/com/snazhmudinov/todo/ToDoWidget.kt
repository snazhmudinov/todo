package com.snazhmudinov.todo

import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import com.snazhmudinov.todo.ToDoWidget.Companion.WIDGET_CALL_EXTRA
import com.snazhmudinov.todo.activity.EditTaskActivity
import com.snazhmudinov.todo.activity.TasksListActivity
import com.snazhmudinov.todo.dao.Task
import com.snazhmudinov.todo.repository.TasksRepository
import dagger.android.AndroidInjection
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

/**
 * Implementation of App Widget functionality.
 */
class ToDoWidget : AppWidgetProvider() {

    private var taskListEmpty = false

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId, taskListEmpty)
        }
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        context ?: return

        if (intent?.action == AppWidgetManager.ACTION_APPWIDGET_UPDATE) {
            // Refresh the widget
            val manager = AppWidgetManager.getInstance(context)
            val ids = manager.getAppWidgetIds(ComponentName(context, ToDoWidget::class.java))

            manager.notifyAppWidgetViewDataChanged(ids, R.id.tasksListView)

            taskListEmpty = intent.getBooleanExtra(TasksListActivity.EMPTY_EXTRA_KEY, true)
        }

        super.onReceive(context, intent)
    }

    companion object {

        const val WIDGET_CALL_EXTRA = "WIDGET_CALL_EXTRA"

        internal fun updateAppWidget(context: Context, appWidgetManager: AppWidgetManager,
                                     appWidgetId: Int,
                                     taskListEmpty: Boolean) {

            // Construct the RemoteViews object
            val views = RemoteViews(context.packageName, R.layout.to_do_widget)

            // Set adapter for the list view
            val intent = Intent(context, ListViewWidgetService::class.java)
            views.setRemoteAdapter(R.id.tasksListView, intent)

            // List view click handler
            val clickIntentTemplate = Intent(context, EditTaskActivity::class.java)
            val clickPendingIntentTemplate = TaskStackBuilder.create(context)
                    .addNextIntentWithParentStack(clickIntentTemplate)
                    .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
            views.setPendingIntentTemplate(R.id.tasksListView, clickPendingIntentTemplate)

            // Toggle view visibility if there are no tasks
            views.setViewVisibility(R.id.tasksListView, if (taskListEmpty) View.GONE else View.VISIBLE)
            views.setViewVisibility(R.id.emptyListLabel, if (taskListEmpty) View.VISIBLE else View.GONE)

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }
}

class ListViewWidgetService : RemoteViewsService() {

    @Inject
    lateinit var tasksRepository: TasksRepository

    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
    }

    override fun onGetViewFactory(intent: Intent?): RemoteViewsFactory {
        return ListProvider(this, tasksRepository)
    }
}

class ListProvider(private val context: Context, private val tasksRepository: TasksRepository)
    : RemoteViewsService.RemoteViewsFactory {

    private var items: List<Task>? = null

    override fun onCreate() = Unit

    override fun getLoadingView(): RemoteViews? = null

    override fun getItemId(position: Int): Long = position.toLong()

    override fun onDataSetChanged() {
        runBlocking {
            items = tasksRepository.getAll()
        }
    }

    override fun hasStableIds(): Boolean = true

    override fun getViewTypeCount(): Int = 1

    override fun onDestroy() = Unit

    override fun getViewAt(position: Int): RemoteViews {
        val item = items?.get(position)
        val remoteViews = RemoteViews(context.packageName, R.layout.todo_list_row)

        remoteViews.setTextViewText(R.id.listItemTitle, item?.title)

        // Set intent with extra
        val fillInIntent = Intent()
        fillInIntent.putExtra(TasksListActivity.TASK_ID_EXTRA, item?.taskId)
        fillInIntent.putExtra(WIDGET_CALL_EXTRA, true)
        remoteViews.setOnClickFillInIntent(R.id.rowContainer, fillInIntent)

        return remoteViews
    }

    override fun getCount(): Int = items?.size ?: 0
}


