package com.snazhmudinov.todo.callback

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.ColorDrawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.snazhmudinov.todo.R

class SwipeToDeleteCallback(private val context: Context, private val callback: SwipeDeleteInterface) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

    private val iconMargin = 30
    private val backgroundColor = ContextCompat.getColor(context, R.color.destructiveRedColor)

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        // Remove item from db
        callback.onDeleteTask(viewHolder.itemId)
    }

    override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
        val background = ColorDrawable(backgroundColor)
        background.setBounds(
                viewHolder.itemView.right + dX.toInt(),
                viewHolder.itemView.top,
                viewHolder.itemView.right,
                viewHolder.itemView.bottom
        )

        val itemView = viewHolder.itemView
        val itemHeight = itemView.bottom - itemView.top

        val icon = ContextCompat.getDrawable(context, R.drawable.ic_delete_black_24dp)

        icon?.setBounds(
                itemView.right - icon.intrinsicWidth - iconMargin,
                (itemHeight - icon.intrinsicHeight) / 2 + itemView.top,
                viewHolder.itemView.right - iconMargin,
                (itemHeight - icon.intrinsicHeight) / 2 + itemView.top + icon.intrinsicHeight
        )

        background.draw(c)
        icon?.draw(c)

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }
}

interface SwipeDeleteInterface {

    fun onDeleteTask(taskId: Long)

}