import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import org.jetbrains.kotlin.konan.properties.Properties
import java.io.FileInputStream

// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    repositories {
        google()
        jcenter()
    }

    dependencies {
        classpath("com.android.tools.build:gradle:${Deps.gradleVersion}")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${Deps.kotlinVersion}")

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle.kts files
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        mavenCentral()
    }
}

plugins {
    id("com.github.ben-manes.versions") version Deps.BEN_MANES_DEPENDENCY_CHECK_VER
}

tasks {
    register("clean", Delete::class) {
        delete(buildDir)
    }

    task("incrementVersionCode") {
        val versionPropsFile = rootProject.file("version.properties")
        val versionProps = Properties()
        versionProps.load(FileInputStream(versionPropsFile))
        val version = versionProps.getProperty("versionCode").toInt() + 1
        Deps.versionCode = version
        versionProps.setProperty("versionCode", version.toString())
        versionProps.store(versionPropsFile.writer(), null)

        println("Increment the version code to $version")
    }

    whenTaskAdded {
        if (name == "assembleRelease") {
            dependsOn("incrementVersionCode")
        }
    }

    named<DependencyUpdatesTask>("dependencyUpdates") {
        resolutionStrategy {
            componentSelection {
                all {
                    val rejected = listOf("alpha", "beta", "rc", "cr", "m", "preview")
                            .map { qualifier -> Regex("(?i).*[.-]$qualifier[.\\d-]*") }
                            .any { it.matches(candidate.version) }
                    if (rejected) {
                        reject("Release candidate")
                    }
                }
            }
        }
        revision = "release"
    }
}
